<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Ota yhteyttä!</title>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col">

                <?php
                    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                        try {
                            $tietokanta = new PDO ('mysql:host=localhost;dbname=mydatabase;charset:utf-8', 'root', '');

                            $sql = 'insert into feedback (name, email, subject, message) values (:name, :email, :subject, :message)';

                            $nimi = filter_input(INPUT_POST, 'nimi', FILTER_SANITIZE_STRING);
                            $sahkoposti = filter_input(INPUT_POST, 'sahkoposti', FILTER_SANITIZE_STRING);
                            $aihe = filter_input(INPUT_POST, 'aihe', FILTER_SANITIZE_STRING);
                            $viesti = filter_input(INPUT_POST, 'viesti', FILTER_SANITIZE_STRING);

                            $kysely = $tietokanta->prepare($sql);
                            $kysely->bindValue(':name', $nimi, PDO::PARAM_STR);
                            $kysely->bindValue(':email', $sahkoposti, PDO::PARAM_STR);
                            $kysely->bindValue(':subject', $aihe, PDO::PARAM_STR);
                            $kysely->bindValue(':message', $viesti, PDO::PARAM_STR);

                            $kysely->execute();
                            print "<p> Kiitos palautteesta! </p>";
                        } 
                        
                        catch (Exception $ex) {
                            print "<p> Tietokantayhteydessä tapahtui virhe! " . $ex->getMessage() . "</p>";
                        }
                    }
                ?>

                <h3>Ota yhteyttä</h3>

                <form action="<?php print $_SERVER['PHP_SELF']; ?>" method="POST">
                    <div class="form-group">
                        <label for="nimi">Nimi:</label>
                        <input type="text" class="form-control" id="nimi" name="nimi">
                    </div>
                    <div class="form-group">
                        <label for="sahkoposti">Sähköposti:</label>
                        <input type="email" class="form-control" id="sahkoposti" name="sahkoposti">
                    </div>
                    <div class="form-group">
                        <label for="otsikko">Viestin otsikko:</label>
                        <input type="text" class="form-control" id="otsikko" name="otsikko">
                    </div>
                    <div class="form-group">
                        <label for="viesti">Viesti:</label>
                        <textarea class="form-control" id="viesti" rows="4" name="viesti"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Lähetä</button>
                </form>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>